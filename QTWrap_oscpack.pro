##   ModulOSC; An open platform for OSC control of analogue devices
##
##   Copyright (C) 2014  Doug Hammond
##
##   This program is free software: you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, either version 3 of the License, or
##   (at your option) any later version.
##
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.
##
##   You should have received a copy of the GNU General Public License
##   along with this program.  If not, see <http://www.gnu.org/licenses/>.

QT       -= core gui

TARGET = oscpack
TEMPLATE = lib

include(../ModulOSC_Project.Cross.pri)

DEFINES += QTWRAP_OSCPACK_LIBRARY

SOURCES += \
	./oscpack-read-only/ip/posix/UdpSocket.cpp \
	./oscpack-read-only/ip/posix/NetworkingUtils.cpp \
	./oscpack-read-only/ip/IpEndpointName.cpp \
	./oscpack-read-only/osc/OscPrintReceivedElements.cpp \
	./oscpack-read-only/osc/OscTypes.cpp \
	./oscpack-read-only/osc/OscReceivedElements.cpp \
	./oscpack-read-only/osc/OscOutboundPacketStream.cpp

HEADERS += \
	./oscpack-read-only/ip/UdpSocket.h \
	./oscpack-read-only/ip/PacketListener.h \
	./oscpack-read-only/ip/NetworkingUtils.h \
	./oscpack-read-only/ip/IpEndpointName.h \
	./oscpack-read-only/ip/TimerListener.h \
	./oscpack-read-only/osc/OscTypes.h \
	./oscpack-read-only/osc/OscException.h \
	./oscpack-read-only/osc/OscPrintReceivedElements.h \
	./oscpack-read-only/osc/OscPacketListener.h \
	./oscpack-read-only/osc/OscOutboundPacketStream.h \
	./oscpack-read-only/osc/OscReceivedElements.h \
	./oscpack-read-only/osc/OscHostEndianness.h \
	./oscpack-read-only/osc/MessageMappingOscPacketListener.h \


INCLUDEPATH += ./oscpack-read-only
DEFINES += OSC_HOST_LITTLE_ENDIAN
